/* 
Purpose: Create table objects in AragonPharmacy database
Script Date: February 18, 2021
Developed by: team
*/

--Human Resources Views
use AragonPharmacy;
go

-- Q.1:

create view HourlyRateAnalysisView
as
select top 100 percent 
	e.empID as 'Employee ID', 
	concat_ws(' ', e.empFirst, isNull(e.empMI, ''), e.empLast) as 'Employee Name', 
	ed.[hourlyWage] as 'Hourly wage'
from [HumanResources].[Employee] as e
	inner join [HumanResources].[EmployeeDetail] as ed 
		on ed.empID = e.empID
where ed.hourlyWage is not null
order by ed.hourlyWage desc;
go

-- Q.2:

create view SpeakSpanishView
as
select  
	e.empID as 'Employee ID', 
	concat_ws(' ', e.empFirst, isnull(e.empMI,''), e.empLast) as 'Employee Name',
	L.LanguageName as 'Language Spoken'
from [HumanResources].[Employee] as e
	inner join HumanResources.EmployeeLanguages as EL 
		on EL.empID = E.empID
	inner join HumanResources.Language as L 
		on L.LangID = EL.LangID
where EL.LangID='Sp';
go


/*
-- expected way
create view SpeakSpanishView
as
select HE.EmpID, concat_ws(' ', HE.EmpFirst, coalesce(EmpMI, ''), HE.EmpLast) as 'Employee Full Name', HE.Memo
from HumanResources.Employee as HE
where HE.Memo like '%Spanish%';
go
*/

--Q.3

create view HourlyRateSummaryView
as
select
	JT.Title as 'Job Title',
	min(ED.hourlyWage) as 'Minimum Wage',max(ED.[hourlyWage]) as 'Maximum Wage'
from [HumanResources].EmployeeDetail as ED
inner join [HumanResources].[Employee] as E 
		on ED.empID = E.empID
	inner join [HumanResources].[JobTitle] as JT 
		on JT.JobID = E.JobID
where ed.hourlyWage is not null
group by JT.Title;
go


-- Q.7a: unmatched records for the employee who do not have any certification

create view EmployeeWithoutCertificationV
as
select distinct EM.EmpID as 'Employee Without Certification', concat_ws(' ', EM.EmpFirst, EM.EmpLast) as 'Full Name'
from HumanResources.Employee as EM
full outer join EmployeeTraining.EmployeeTraining as ET
on ET.EmpID = EM.EmpID;
go

-- Q.7b: unmatched records for the customers who have not ordered any products

-- not applicable in our case
create view CustomersWithNoPrescription ([Customer ID], [Customer Namer])
as
select 
	C.CustID , 
	concat_ws(' ', C.CustFirst, C.CustLast)
from Customers.Customers as C
full outer join Prescription.Rx as R
on C.CustID = R.CustID;
go

--Q.8: Carpool
create view CarpoolView
as
select concat_ws(' ', EM.EmpFirst, EM.EmpLast) as 'Full Name', C.CityName as 'City Name'
from HumanResources.Employee as EM
inner join Humanresources.EmployeeDetail as ED
on Em.EmpID = ED.EmpID
inner join City as C
on ED.CityID = C.CityID
order by C.CityName;
go

-- Q.9 in functions file

-- Q.10 Min, Max, Avg hourly rate

create view MaxMinAvgHourlyRateView
as
select 
	HJ.Title,
	min(HED.hourlyWage) as 'Minimum Hourly wage', 
	max(HED.hourlyWage) as 'Maximum Hourly wage', 
	avg(HED.hourlyWage) as 'Average Hourly wage'
from 
	HumanResources.JobTitle as HJ 
		inner join HumanResources.Employee as HE
			on HJ.JobID = HE.JobID
		inner join HumanResources.EmployeeDetail as HED
			on HE.EmpID = HED.EmpID
where 
	HED.hourlyWage is not null AND HE.jobID in(3,4) -- no technician
Group by HJ.title;
go


<<<<<<< HEAD
---PAGE 30--------------------
--EmployeeClassesView
---Analizing Data from more than one table
create view humanresources.EmployeeClassesView
as
select EM.Emplast, EM.EmpFirst, ET.ClassID, ET.ClassDate
from humanresources.employee as EM
inner join EmployeeTraining.EmployeeTraining as ET
on ET.EmpID = Em.EmpID
;
go
--EmployeeClassesDescriptionView
create view  humanresources.EmployeeClassesDescriptionView
as
select EM.Emplast, EM.EmpFirst, ET.ClassDate, CL.ClassDescription
from humanresources.employee as EM
inner join EmployeeTraining.EmployeeTraining as ET
on ET.EmpID = Em.EmpID
inner join EmployeeTraining.Class as CL
on CL.CLassID = ET.CLassID
order by EM.EmpLast asc
;
go

--EmployeeTrainingView
--- Different from above uses left join to find all records
create view  humanresources.EmployeeTrainingView
as
select EM.Emplast, EM.EmpFirst, CL.ClassDescription
from humanresources.employee as EM
left outer join EmployeeTraining.EmployeeTraining as ET
on ET.EmpID = Em.EmpID
full outer join EmployeeTraining.Class as CL
on CL.CLassID = ET.CLassID
;
go

--upToDateView
create view  humanresources.upToDateView
as
select EM.Emplast, EM.EmpFirst, ET.ClassDate, CL.ClassDescription
from humanresources.employee as EM
left outer join EmployeeTraining.EmployeeTraining as ET
on ET.EmpID = Em.EmpID
full outer join EmployeeTraining.Class as CL
on CL.CLassID = ET.CLassID
;
go

--Renewal Info
create view humanresources.RenewalInfo
as
select EM.Emplast, EM.EmpFirst, ET.ClassDate, CL.ClassDescription
from humanresources.employee as EM
left outer join EmployeeTraining.EmployeeTraining as ET
on ET.EmpID = Em.EmpID
full outer join EmployeeTraining.Class as CL
on CL.CLassID = ET.CLassID
where CL.ClassID in(1,3)
and ET.ClassDate between '1/1/2019' and '12/31/2019'
;
go
=======
-- Q 11: Avoiding Duplicate Records

select c.CustID as 'Customer ID',c.CustFirst as 'First Name', c.CustLast 'Last Name', concat_ws(',',h.[Address],city.CityName,h.Province, h.country,h.postalCode )as 'Address'
from [Customers].[Customers] as c
inner join [Customers].[HouseHold] as H on H.HouseID = C.HouseID
inner join city on city.cityID = H.CityID
where c.headHH =1;
>>>>>>> 9a3b475194874ce033426857fc2245760a0afd80
