/* ********** function page *********** */

use AragonPharmacy;
go


-- Q. Years of service - Page 31

create function HumanResources.YearsOfServiceFn
(
	-- declare a parameter
	-- Syntax: @parameter_name data_type [ = expression]
	@YearsOfService as datetime
)
-- returns data_type
returns int
as
	begin
		-- declare the return variable 
		-- declare @variable_name as data_type [=expression]
		declare @seniority as int
		-- compute the returned value 
		select @seniority = abs(dateDiff(year, @YearsOfService, getDate() ))
		-- return the result value to the function caller 
		return @seniority
	end
;
go

select HumanResources.YearsOfServiceFn('2001/02/17')
;
go

select
	HED.EmpID,
	HumanResources.YearsOfServiceFn(HED.startDate) as 'Seniority'
from HumanResources.EmployeeDetail as HED
;
go

--Q.9: Using Parameter Values

create function HumanResources.getSubstituteListFn
(
	@JobID as tinyint
)
returns table
return(
		select top 100 percent  e.empLast as'Employee Last Name', e.EmpFirst as 'Employee First Name',  e.PrimaryPhone as 'Home Phone', e.SecondaryPhone as 'Cellphone', e.JobID as 'JobID', ed.EndDate
		from [HumanResources].[Employee] as e
		inner join [HumanResources].[EmployeeDetail] as ed on e.empID = ed.empID
		where jobID = @jobID and ed.EndDate is null
		order by EmpLast
	)
;
go
------Test Function-----
select *
from [HumanResources].[getSubstituteListFn](2);
go








