/* 
Purpose: Create table objects in AragonPharmacy database
Script Date: February 13, 2021
Developed by: Emma
*/


use AragonPharmacy
;
go

/* *** Table No.1 - Customers.Customers *** */

if OBJECT_ID('Customers.Customers', 'U') is not null
    drop table Customers.Customers;
go

CREATE TABLE Customers.Customers (

    --column_name data_type contraint_type
    CustID int identity(1,1) not null, -- PK
	CustFirst nvarchar(20) not null,
	CustLast nvarchar(20) not null,
	PrimaryPhone nchar(15) not null,
	DOB datetime not null,
	Gender nchar(1) not null,
	ChildproofCap bit not null,
	HouseID int not null, -- FK Household
	HeadHH bit, -- 0 or 1 yes or no
	AllergyNote nvarchar(100) null,
	PharmacyMemberSince datetime null,
	Fax nchar(15) null

    -- constraint constraint_name constraint_type [clustered] (pk [asc-desc])
    CONSTRAINT pk_Customers PRIMARY KEY clustered (CustID asc)
);
go

/* *** Table No.2 - Customers.CustomerDetails *** */

if OBJECT_ID('Customers.CustomerDetails', 'U') is not null
    drop table Customers.CustomerDetails;
go

CREATE TABLE Customers.CustomerDetails (

	CustDetailID int identity(1,1) not null, -- PK
	CustID int not null, -- FK Customers
	SIN nchar(11) not null,
	Employer nvarchar(30) null,
	Occupation nvarchar(30) null,
	MaritalStatusID nchar(1) not null, -- FK MaritalStatus
	SpouseFirst nvarchar(20) null,
	SpouseLast nvarchar(20) null,
	AcceptTermsConditions bit not null,
	Balance money not null,
	PlanID int not null, -- FK HealthPlan (smallint(5) ??)
	DoctorID smallint null, -- FK Clinic
	EmergencyContactFirst nvarchar(20) null,
	EmergencyContactLast nvarchar(20) null,
	RelationshipToPatient nvarchar(20) null,
	ContactPhone nchar(15) null

	CONSTRAINT pk_CustomerDetails PRIMARY KEY clustered (CustDetailID asc)
);
go

/* *** Table No.3 - Customers.MaritalStatus *** */

if OBJECT_ID('Customers.MaritalStatus', 'U') is not null
    drop table Customers.MaritalStatus;
go

CREATE TABLE Customers.MaritalStatus (

	StatusID nchar(1) not null, -- PK
	Description nvarchar(15) not null

	CONSTRAINT pk_MaritalStatus PRIMARY KEY clustered (StatusID asc)
);
go

/* *** Table No.4 - Customers.HealthPlan *** */

if OBJECT_ID('Customers.HealthPlan', 'U') is not null
    drop table Customers.HealthPlan;
go

CREATE TABLE Customers.HealthPlan (

	PlanID int identity(1,1) not null, -- PK
	PlanName nvarchar(50) not null,
	InsuranceNo nvarchar(20) not null,
	Address nvarchar(50) not null,
	CityID nvarchar(6) not null, -- FK City, default(Mtl)
	Province nchar(2) not null, -- default(QC)
	Country nchar(6) not null, -- default(Canada)
	PostalCode nchar(7) not null,
	Phone nchar(15) not null,
	website nvarchar(50) null

	CONSTRAINT pk_HealthPlan PRIMARY KEY clustered (PlanID asc)
);
go

/* *** Table No.5 - Customers.HouseHold *** */

if OBJECT_ID('Customers.HouseHold', 'U') is not null
    drop table Customers.HouseHold;
go

CREATE TABLE Customers.HouseHold (

	HouseID int identity(1,1) not null, -- PK
	HHPhone nchar(15) null, 
	Address nvarchar(50) not null,
	CityID nvarchar(6) not null, -- FK City, default(Mtl)
	Province nchar(2) not null, -- defult(Qc)
	Country nchar(6) not null, -- default(Canada)
	PostalCode nchar(7) not null

	CONSTRAINT pk_HouseHold PRIMARY KEY (HouseID asc)
);
go

/* *** Table No.6 - Customers.Allergy *** */

if OBJECT_ID('Customers.Allergy', 'U') is not null
    drop table Customers.Allergy;
go

CREATE TABLE Customers.Allergy (

	CustID int not null, -- C.PK Customer
	AllergicTo nvarchar(20) not null -- C.PK

	CONSTRAINT pk_Allergy PRIMARY KEY
		(
			CustID asc,
			AllergicTo asc
		)
);
go

/* *** Table No.7 - Operations.Suppliers *** */

if OBJECT_ID('Operations.Suppliers', 'U') is not null
    drop table Operations.Suppliers;
go

CREATE TABLE Operations.Suppliers (

	SupplierID int identity(1,1) not null, -- PK
	Name nvarchar(40) not null,
	Phone nchar(15) not null,
	Address nvarchar(50) not null,
	CityID nvarchar(6) not null, -- FK City, default(Mtl)
	Province nchar(2) not null, -- defult(Qc)
	Country nchar(6) not null, -- default(Canada),
	PostalCode nchar(7) not null,
	Email nvarchar(30) null,
	Website nvarchar(50) null

	CONSTRAINT pk_Suppliers PRIMARY KEY (SupplierID asc)
);
go 

/************************** KEVIN ******************************************************/

-- create Employee table

if OBJECT_ID('HumanResources.Employee', 'U') is not null
    drop table HumanResources.Employee;
go

create table HumanResources.Employee(
EmpID int not null identity(1,1),
EmpFirst nvarchar(30) not null,
EmpMI nvarchar(2) null,
EmpLast nvarchar(30) not null,
JobID tinyint not null,
Memo nvarchar(300) not null,
PrimaryPhone nchar(15) not null,
SecondaryPhone nvarchar(30) null,
CONSTRAINT pk_Employee PRIMARY KEY (EmpID asc)
);
go

--create EmployeeDetail table

if OBJECT_ID('HumanResources.EmployeeDetail', 'U') is not null
    drop table HumanResources.EmployeeDetail;
go

create table HumanResources.EmployeeDetail(
EmpDetailID int not null identity(1,1),
EmpID int not null,
SIN nchar(11) not null,
DOB datetime not null,
startDate datetime not null,
EndDate datetime null,
Address nvarchar(30) not null,
CityID nvarchar(6) not null,
Province nchar(2) not null,
country nvarchar(6) not null,
postalCode nchar(7) not null,
Salary money null,
hourlyWage money null,
lastReview datetime null,
UpcomingReview datetime null,
ReviewNotes nvarchar(300) null
CONSTRAINT pk_EmployeeDetail PRIMARY KEY (EmpDetailID asc)
);
go

--Create Language Table

if OBJECT_ID('HumanResources.Language', 'U') is not null
    drop table HumanResources.Language;
go

create table HumanResources.Language(
LangID nvarchar(3) not null,
LanguageName nvarchar(30) not null
CONSTRAINT PK_Language PRIMARY KEY (LangID asc)
);
go

-- create Proficiency Table

if OBJECT_ID('HumanResources.Proficiency', 'U') is not null
    drop table HumanResources.Proficiency;
go

create table HumanResources.Proficiency(
ProficiencyID nvarchar(1) not null,
Level nvarchar(20) not null
CONSTRAINT PK_Proficiency PRIMARY KEY (ProficiencyID asc)
);
go


-- create EmployeeLanguages Table

if OBJECT_ID('HumanResources.EmployeeLanguages', 'U') is not null
    drop table HumanResources.EmployeeLanguages;
go

create table HumanResources.EmployeeLanguages(
EmpID int not null,
LangID nvarchar(3) not null,
ProficiencyID nvarchar(1) not null
CONSTRAINT PK_EmployeeLanguages PRIMARY KEY (EmpID asc, LangID asc)
);
go

-- create EmployeeAbsentee Table

if OBJECT_ID('HumanResources.EmployeeAbsentee', 'U') is not null
    drop table HumanResources.EmployeeAbsentee;
go

create table HumanResources.EmployeeAbsentee(
EmpID int not null,
dateAbsentee datetime not null,
absent bit not null,
tardy bit not null
CONSTRAINT PK_EmployeeAbsentee PRIMARY KEY (EmpID asc,dateAbsentee asc)
);
go

-- create Proficiency Table

if OBJECT_ID('HumanResources.JobTitle', 'U') is not null
    drop table HumanResources.JobTitle;
go

create table HumanResources.JobTitle(
JobID tinyint not null,
Title nvarchar(30) not null
CONSTRAINT PK_JobTitle PRIMARY KEY (JobID asc)
);
go

-- create EmployeeSickDays Table

if OBJECT_ID('HumanResources.EmployeeSickDays', 'U') is not null
    drop table HumanResources.EmployeeSickDays;
go

create table HumanResources.EmployeeSickDays(
EmpID int not null,
daysAllowed tinyint not null,
daysTaken tinyint not null
CONSTRAINT PK_EmployeeSickDays PRIMARY KEY (EmpID asc)
);
go

-- create EmployeeVacationDays Table

if OBJECT_ID('HumanResources.EmployeeVacationDays', 'U') is not null
    drop table HumanResources.EmployeeVacationDays;
go

create table HumanResources.EmployeeVacationDays(
EmpID int not null,
daysAllowed tinyint not null,
daysTaken tinyint not null,
CONSTRAINT PK_EmployeeVacationDays PRIMARY KEY (EmpID asc)
);
go

--create EmployeeTraining Table

if OBJECT_ID('EmployeeTraining.EmployeeTraining', 'U') is not null
    drop table EmployeeTraining.EmployeeTraining;
go

create table EmployeeTraining.EmployeeTraining(
EmpID int not null,
ClassID int not null,
ClassDate datetime not null
CONSTRAINT PK_EmployeeTraining PRIMARY KEY (EmpID asc, classID asc, ClassDate asc)
);
go

 --create Class Table

 if OBJECT_ID('EmployeeTraining.class', 'U') is not null
    drop table EmployeeTraining.class;
go

create table EmployeeTraining.class(
ClassID int not null identity(1,1),
ClassDescription nvarchar(50) null,
cost money not null,
Renewal tinyint not null,
RequiredClass bit not null,
ClassProvider nvarchar(30) not null
CONSTRAINT PK_class PRIMARY KEY (classID asc)
);
go

/* ********************** MALIK ******************************* */

-- *****************  TABLE MEDICAL.DOCTOR ******************

if OBJECT_ID('Medical.Doctor', 'U') is not null
    drop table Medical.Doctor;
go


create table Medical.Doctor
(
	DoctorID smallint identity(1,1) not null,  
	DoctorFirst nvarchar(30) not null,
	DoctorLast nvarchar(30) not null,
	PrimaryPhone nchar(15) not null,
	SecondaryPhone nchar(15) null,
	ClinicID smallint not null
	constraint pk_Doctor primary key clustered (DoctorID asc)
)
;
go

-- *****************  TABLE MEDICAL.CLINIC ******************

if OBJECT_ID('Medical.Clinic', 'U') is not null
    drop table Medical.Clinic;
go

create table Medical.Clinic
(
	ClinicID smallint identity(1,1) not null,  
	ClinicName nvarchar(50) not null,
	Address1 nvarchar(40) not null,
	Address2 nvarchar(40) null,
	CityID nvarchar(6) not null,	
	Province nchar(2) not null,
	Country nvarchar(6),
	PostalCode nchar(7) not null,
	Phone nchar(15) not null
	constraint pk_Clinic primary key clustered (ClinicID asc)
)
;
go

-- *****************  TABLE DRUGS.DRUG ******************

if OBJECT_ID('Drugs.Drug', 'U') is not null
    drop table Drugs.Drug;
go

create table Drugs.Drug
(
	DIN nchar(8) not null,
	DrugName nvarchar(30) not null,      
	IsGeneric bit not null,
	DrugDescription nvarchar(100) not null,
	Unit nvarchar(10) not null,
	Dosage nvarchar(10) not null,
	DosageForm nvarchar(20) not null,
	Cost money not null,
	Price money not null,
	interactions nvarchar(25) not null,	
	Quantity decimal(5,2) not null,		
	constraint pk_Drugs primary key clustered (DIN asc)
)
;
go

-- *****************  TABLE PRESCRIPTION.REFILL ******************

if OBJECT_ID('Prescription.Refill', 'U') is not null
    drop table Prescription.Refill;
go

create table Prescription.Refill
(
	PrescriptionID int identity(1,1) not null,	
	EmplID int not null,
	RefillDate datetime not null,
	constraint pk_Refill primary key clustered (PrescriptionID asc, EmplID asc)
)
;
go

-- *****************  TABLE PRESCRIPTION.RX ******************

if OBJECT_ID('Prescription.Rx', 'U') is not null
    drop table Prescription.Rx;
go

create table Prescription.Rx
(
	PrescriptionID int identity(1,1) not null,
	DIN nchar(8) not null,
	CustID int not null,
	DoctorID smallint not null,				
	Quantity decimal(5,2) not null,
	Unit nvarchar(10) not null,
	RefillDate datetime not null,
	ExpireDate datetime not null,
	Refills tinyint not null,
	AutoRefills bit not null,
	RefillsUsed tinyint not null,
	Instructions nvarchar(50) not null,
	constraint pk_Rx primary key clustered (PrescriptionID asc)
)
;
go


if OBJECT_ID('City', 'U') is not null
    drop table City;
go


create table City(
CityID nvarchar(6),
CityName nvarchar(30)
constraint pk_City primary key clustered (CityID asc)
);
go


ALTER TABLE [Medical].[Clinic]
ALTER COLUMN [ClinicName] nvarchar(50);
go