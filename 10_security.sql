/* 
Purpose: Create table objects in AragonPharmacy database
Script Date: February 18, 2021
Developed by: team
*/

-- Security on HumanResources.EmployeeDetails

deny
	select,
	insert,
	update,
	delete
on table::HumanResources.EmployeeDetails
to TestUser; -- TestUser: any futur db user that are NOT part of the HumanResources schema.


-- Security on HumanResources.EmployeeAbsentee

deny
	select,
	insert,
	update,
	delete
on table::HumanResources.EmployeeAbsentee
to TestUser; -- TestUser: any futur db user that are NOT part of the HumanResources schema.


-- Security on HumanResources.EmployeeSickDays

deny
	select,
	insert,
	update,
	delete
on table::HumanResources.EmployeeSickDays
to TestUser; -- TestUser: any futur db user that are NOT part of the HumanResources schema.


-- Security on HumanResources.EmployeeVacationDays

deny
	select,
	insert,
	update,
	delete
on table::HumanResources.EmployeeVacationDays
to TestUser; -- TestUser: any futur db user that are NOT part of the HumanResources schema.


-- Security on EmployeeTraining schema

deny
	select,
	insert,
	update,
	delete
on schema::EmployeeTraining
to TestUser; -- TestUser: any futur db user that are NOT part of the HumanResources schema.

-- Security on Drugs schema

deny
	insert,
	update,
	delete
on schema::Drugs
to TestUser; -- TestUser: any futur db user that are NOT pharmacists or pharmsacist technicians.


-- Security on Prescription schema

deny
	select,
	insert,
	update,
	delete
on schema::Drugs
to TestUser; -- TestUser: any futur db user that are NOT pharmacists or pharmacist technicians.


-- Security on Customers schema

deny
	select,
	insert,
	update,
	delete
on schema::Customers
to TestUser; -- TestUser: any futur db user that are NOT pharmacists or pharmacist technicians and part of the HumanResource schema.


-- Security on Operations schema

deny
	insert,
	update,
	delete
on schema::Operations
to TestUser; -- TestUser: any futur db user that are NOT pharmacists or pharmacist technicians and part of the HumanResource schema.


-- Security on Medical schema

deny
	select,
	insert,
	update,
	delete
on schema::Medical
to TestUser; -- TestUser: any futur db user that are NOT pharmacists or pharmacist technicians.
