/* 
Purpose: Apply foreign keys to AragonPharmacy database
Script Date: February 15, 2021
Developed by: Diego
*/

-- This file is not executed yet!!

use AragonPharmacy
;
go


/* **************************************************** HumanResources Schema **************************************************** */

/* ************************** Employee table ************************** */

/* **** paired with HumanResources.JobTitle Table **** */
alter table HumanResources.Employee
	add constraint fk_Employee_JobTitle foreign key (JobID)
		references HumanResources.JobTitle (JobID)
;
go


/* ************************** EmployeeDetail table ************************** */

/* **** paired with HumanResources.Employee Table **** */
alter table HumanResources.EmployeeDetail
	add constraint fk_EmployeeDetail_Employee foreign key (EmpID)
		references HumanResources.Employee (EmpID)
;
go


/* **** paired with DBO.City Table **** */
alter table HumanResources.EmployeeDetail
	add constraint fk_EmployeeDetail_City foreign key (CityID)
		references dbo.City (CityID)
;
go


/* ************************** EmployeeLanguage table ************************** */

/* **** paired with HumanResources.Employee Table **** */
alter table HumanResources.EmployeeLanguages
	add constraint fk_EmployeeLanguages_Employee foreign key (EmpID)
		references	HumanResources.Employee (EmpID)
;
go


/* **** paired with HumanResources.Language Table **** */
alter table HumanResources.EmployeeLanguages
	add constraint fk_EmployeeLanguages_Language foreign key (LangID)
		references HumanResources.Language (LangID)
;
go

/* **** paired with HumanResources.Proficiency Table **** */
alter table HumanResources.EmployeeLanguages
	add constraint fk_EmployeeLanguages_Proficiency foreign key (ProficiencyID)
		references HumanResources.Proficiency (ProficiencyID)
;
go


/* ************************** Language table ************************** */

/* ** table with no foreign keys ** */


/* ************************** Proficiency table ************************** */

/* ** table with no foreign keys ** */


/* ************************** EmployeeAbsentee table ************************** */

/* **** paired with HumanResources.Employee Table **** */
alter table HumanResources.EmployeeAbsentee
	add constraint fk_EmployeeAbsentee_Employee foreign key (EmpID)
		references HumanResources.Employee (EmpID)
;
go


/* ************************** JobTitle table ************************** */

/* ** table with no foreign keys ** */


/* ************************** EmployeeickDays table ************************** */

/* **** paired with HumanResources.Employee Table **** */
alter table HumanResources.EmployeeSickDays
	add constraint fk_EmployeeSickDays_Employee foreign key (EmpID)
		references HumanResources.Employee (EmpID)
;
go


/* ************************** EmployeeVacationDays table ************************** */

/* **** paired with HumanResources.Employee Table **** */
alter table HumanResources.EmployeeVacationDays
	add constraint fk_EmployeeVacationDays_Employee foreign key (EmpID)
		references HumanResources.Employee (EmpID)
;
go


/* **************************************************** EmployeeTraining Schema **************************************************** */

/* ************************** EmployeeTraining table ************************** */

/* **** paired with HumanResources.Employee Table **** */
alter table EmployeeTraining.EmployeeTraining
	add constraint fk_EmployeeTraining_Employee foreign key (EmpID)
		references HumanResources.Employee(EmpID)
;
go

/* **** paired with EmployeeTraining.Class Table **** */
alter table EmployeeTraining.EmployeeTraining
	add constraint fk_EmployeeTraining_Class foreign key (ClassID)
		references EmployeeTraining.Class(ClassID)
;
go


/* ************************** Class table ************************** */

/* ** table with no foreign keys ** */


/* **************************************************** Drugs Schema **************************************************** */

/* ************************** Drug Table ************************** */

/* ** table with no foreign keys ** */


/* **************************************************** Prescription Schema **************************************************** */

/* ************************** Refill table ************************** */

/* **** paired with Prescription.Rx Table **** */
alter table Prescription.Refill
	add constraint fk_Refill_Rx foreign key (PrescriptionID)
		references Prescription.Rx (PrescriptionID)
;
go

/* **** paired with HumanResources.Employee Table **** */
alter table Prescription.Refill
	add constraint fk_Refill_Employee foreign key (EmplID)
		references HumanResources.Employee (EmpID)
;
go

/* ************************** Rx table ************************** */

/* **** paired with Drugs.Drug Table **** */
alter table Prescription.Rx
	add constraint fk_Rx_Drug foreign key (DIN)
		references Drugs.Drug (DIN)
;
go

/* **** paired with Customers.Customers Table **** */
alter table Prescription.Rx
	add constraint fk_Rx_Customers foreign key (CustID)
		references Customers.Customers (CustID)
;
go

/* **** paired with Medical.Doctor Table **** */
alter table Prescription.Rx
	add constraint fk_Rx_Doctor foreign key (DoctorID)
		references Medical.Doctor (DoctorID)
;
go


/* **************************************************** Customers Schema **************************************************** */

/* ************************** Customers table ************************** */

/* **** paired with Customers.Household Table **** */
-- self referring
alter table Customers.Customers
	add constraint fk_Customers_Household foreign key (HouseID)
		references Customers.Household (HouseID)
;
go



/* ************************** CustomerDetail table ************************** */

/* **** paired with Customers.Customers Table **** */
alter table Customers.CustomerDetails
	add constraint fk_CustomerDetails_Customers foreign key (CustID)
		references Customers.Customers(CustID)
;
go

/* **** paired with Customers.MaritalStatus Table **** */
alter table Customers.CustomerDetails
	add constraint fk_CustomerDetails_MaritalStatus foreign key (MaritalStatusID)
		references Customers.MaritalStatus(StatusID)
;
go

/* **** paired with Customers.HealthPlan Table **** */
alter table Customers.CustomerDetails
	add constraint fk_CustomerDetails_HealthPlan foreign key (PlanID)
		references Customers.HealthPlan(PlanID)
;
go -- data type error

/* **** paired with Medical.Doctor Table **** */
alter table Customers.CustomerDetails
	add constraint fk_CustomerDetails_Doctor foreign key (DoctorID)
		references Medical.Doctor(DoctorID)
;
go


/* ************************** MaritalStatus table ************************** */

/* ** table with no foreign keys ** */


/* ************************** HealthPlan table ************************** */

/* **** paired with DBO.City Table **** */
alter table Customers.HealthPlan
	add constraint fk_HealthPlan_City foreign key (CityID)
		references DBO.City	(CityID)
;
go


/* ************************** Household table ************************** */

/* **** paired with DBO.City Table **** */
alter table Customers.Household
	add constraint fk_Household_City foreign key (CityID)
		references DBO.City	(CityID)
;
go


/* ************************** Allergy table ************************** */

/* **** paired with Customers.Customers Table **** */
alter table Customers.Allergy
	add constraint fk_Allergy_Customers foreign key (CustID)
		references Customers.Customers (CustID)
;
go


/* **************************************************** Operations Schema **************************************************** */

/* ************************** Suppliers table ************************** */

/* **** paired with DBO.City Table **** */
alter table Operations.Suppliers
	add constraint fk_Suppliers_City foreign key (CityID)
		references DBO.City (CityID)
;
go


/* **************************************************** Medical Schema **************************************************** */

/* ************************** Doctor table ************************** */

/* **** paired with Medical.Clinic Table **** */
alter table Medical.Doctor
	add constraint fk_Doctor_Clinic foreign key (ClinicID)
		references Medical.Clinic (ClinicID)
;
go


/* ************************** Clinic table ************************** */

/* **** paired with DBO.City Table **** */
alter table Medical.Clinic
	add constraint fk_Clinic_City foreign key (CityID)
		references DBO.City	(CityID)
;
go


/* **************************************************** DBO  Schema **************************************************** */

/* ************************** City table ************************** */

/* ** table with no foreign keys ** */



















