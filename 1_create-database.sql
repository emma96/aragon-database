/* 
Purpose: Create AragonPharmacy database
Script Date: February 15, 2021
Developed by: Emma
*/

-- this file is not executed yet!!

use master
;
go

/*
create object_type object_name
create database database_name
*/

create database AragonPharmacy
on PRIMARY(
    -- 1)rows data logical file_name
    name = 'AragonPharmacy',
    -- 2) rows data initial file_size
    size = 500MB, -- 12000KB, any unit works
    -- 3) rows data auto growth_size
    filegrowth = 10%,
    -- 4) rows data maximum file_size
    maxsize = unlimited, -- unlimited (keeps growing, but you need to make sure you have enough space on drive)
    -- 5) rows data path and file_name
    filename = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\AragonPharmacy.mdf'
)
log on 
(
    -- 1) log logical file_name
    name = 'AragonPharmacy_log',
    -- 2) log initial file_size (1/4 of data file_size)
    size = 125MB,
    -- 3)log auto growth_size (for log, in % unit)
    filegrowth = 10%,
    -- 4)log maximum file_size
    maxsize = unlimited,
    -- 5)log path and file_name
    filename = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\AragonPharmacy_log.ldf'
);
go