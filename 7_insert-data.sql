

/* ********************************************************** Dummy Data Links ********************************************************** */

use AragonPharmacy
;
go

/*
bulk insert schema_name.table_name
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go
*/

/* **************************************************** HumanResources Schema **************************************************** */

/* ************************** Employees table ************************** */

bulk insert HumanResources.Employee
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\Employee.csv'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go -- em : done

select * 
from HumanResources.Employee;
go

/* ************************** EmployeeDetail table ************************** */    

bulk insert HumanResources.EmployeeDetail 
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\EmployeeDetailAddon.csv'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go -- em: done

select *
from HumanResources.EmployeeDetail;
go

/* ************************** EmployeeLanguages table ************************** */

bulk insert HumanResources.EmployeeLanguages
from 'C:\Users\edb13\OneDrive\Documents\aragon-database\Data Files\EmployeeLanguages.csv'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go -- em: done

select * 
from HumanResources.EmployeeLanguages;
go

/* ************************** Languages table ************************** */

bulk insert HumanResources.Language
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\Languages_dummydata.txt'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* ************************** Proficiency table ************************** */

bulk insert HumanResources.Proficiency
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\Proficiency_dummydata.txt'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go


/* ************************** EmployeeAbsentee table ************************** */

bulk insert HumanResources.EmployeeAbsentee
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\EmployeeAbsentee_dummydata.csv'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* ************************** JobTitle table ************************** */

bulk insert HumanResources.JobTitle
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\JobTitleAddon.csv'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* ************************** EmployeeSickDays table ************************** */

bulk insert HumanResources.EmployeeSickDays
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\EmployeeSickDays_dummydata.csv'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* ************************** EmployeeVacationDays table ************************** */

bulk insert HumanResources.EmployeeVacationDays
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\EmployeeVacationDays_dummydata.txt'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* **************************************************** EmployeeTraining Schema **************************************************** */

/* ************************** EmployeeTraining table ************************** */

bulk insert EmployeeTraining.EmployeeTraining
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\EmployeeTraining.txt'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* ************************** Class table ************************** */

bulk insert EmployeeTraining.Class
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\Class.txt'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* **************************************************** Drugs Schema **************************************************** */

/* ************************** Drug Table ************************** */

bulk insert Drugs.Drug
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\DrugModified.csv'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* **************************************************** Prescription Schema **************************************************** */

/* ************************** Refill table ************************** */

bulk insert Prescription.Refill
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\Refill.csv'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* ************************** Rx table ************************** */

bulk insert Prescription.Rx
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\Rx.csv'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* **************************************************** Customers Schema **************************************************** */

/* ************************** Customers table ************************** */

bulk insert Customers.Customers
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\CustomerModified.csv'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* ************************** CustomerDetails table ************************** */

bulk insert Customers.CustomerDetails
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\CustomerDetailsAddon.csv'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* ************************** MaritalStatus table ************************** */

bulk insert Customers.MaritalStatus
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\MaritalStatus.txt'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* ************************** HealthPlan table ************************** */

bulk insert Customers.HealthPlan
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\HealthPlanModified.csv'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* ************************** Household table ************************** */

bulk insert Customers.Household
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\HouseholdModified.csv'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* ************************** Allergy table ************************** */

bulk insert Customers.Allergy
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\AllergyAddon.csv'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* **************************************************** Operations Schema **************************************************** */

/* ************************** Suppliers table ************************** */

bulk insert Operations.Suppliers
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\SupplierAddon.csv'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* **************************************************** Medical Schema **************************************************** */

/* ************************** Doctor table ************************** */

bulk insert Medical.Doctor
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\Doctor.csv'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go

/* ************************** Clinic table ************************** */

bulk insert Medical.Clinic
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\ClinicModified.csv'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go



/* **************************************************** DBO  Schema **************************************************** */

/* ************************** City table ************************** */

bulk insert DBO.City
from 'C:\Users\Diego Marco\Desktop\Class\Class9_Database2\Class9_inClassProject\TeamProject_Deposit\Data Files\City_dummydata.txt'
with
(
	FirstRow = 2,
	RowTerminator = '\n',
	FieldTerminator = ','
)
;
go





