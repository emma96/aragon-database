/* 
Purpose: Create Indexes in the AragonPharmacy database
Script Date: February 15, 2021
Developed by: Emma
*/
use [AragonPharmacy];

use AragonPharmacy
;
go


-- 1. Create Index on the CustLast field in tblCustomers.Customers

EXEC sp_helpindex 'Customers.Customers';
go

-- non-clustered index
create NONCLUSTERED index ncl_CustLast on Customers.Customers (CustLast);
go


-- 2. Create Index on the Name field in tblDrugs.Drug

EXEC sp_helpindex 'Drugs.Drug';
go

-- non-clustered index
create NONCLUSTERED index ncl_Name on Drugs.Drug (DrugName);
go