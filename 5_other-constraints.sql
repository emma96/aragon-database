/* 
Purpose: Apply other constraints (data-integrity) to AragonPharmacy database
Script Date: February 15, 2021
Developed by: Diego
*/

-- This file is not executed yet!!

use AragonPharmacy
;
go


/* **************************************************** HumanResources Schema **************************************************** */

/* ************************** Employee table ************************** */


--Regular expression Phonenumber
alter table HumanResources.Employee
	add constraint ck_PrimaryPhone_Employee check ( PrimaryPhone like '^\([0-9]^3\) [0-9]^3-[0-9]^4$')
;
go

alter table HumanResources.Employee
	add constraint ck_SecondaryPhone_Employee check ( SecondaryPhone like '^\([0-9]^3\) [0-9]^3-[0-9]^4$')
;
go


/* ************************** EmployeeDetail table ************************** */
-- Check EndDate > StartDate
alter table HumanResources.EmployeeDetail
	add constraint ck_EndDate_EmployeeDetail check (EndDate > StartDate)
;
go

-- Default CityId = Mtl
alter table HumanResources.EmployeeDetail
	add constraint df_CityID_EmployeeDetail default ('Mtl') for CityID
;
go

-- Default Province = Qc
alter table HumanResources.EmployeeDetail
	add constraint df_Province_EmployeeDetail default ('Qc') for Province
;
go

-- Default Country = Canada
alter table HumanResources.EmployeeDetail
	add constraint df_Country_EmployeeDetail default ('Canada') for Country
;
go

--Regular expression PostalCode
alter table HumanResources.EmployeeDetail
	add constraint ck_PostalCode_EmployeeDetail check (PostalCode like '^[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][abceghjklmnprstvwxyzABCEGHJKLMNPRSTVWXYZ] [0-9][abceghjklmnprstvwxyzABCEGHJKLMNPRSTVWXYZ][0-9]$')
;
go

alter table HumanResources.EmployeeDetail
	add constraint ck_Sin_EmployeeDetail check ( Sin like '^[0-9]^3-[0-9]^3-[0-9]^3$')
;
go

alter table HumanResources.EmployeeDetail
	add constraint ck_Province_EmployeeDetail check (Province in ('NL','PE','NS','NB', 'QC','ON', 'NU','MB', 'SK','AB', 'BC', 'YT', 'NT'))
;
go

/* ************************** EmployeeLanguages table ************************** */

/* ** table with no other constraints ** */


/* ************************** Languages table ************************** */

/* ** table with no other constraints ** */


/* ************************** Proficiency table ************************** */

/* ** table with no other constraints ** */


/* ************************** EmployeeAbsentee table ************************** */
-- Check Absent != Tardy
alter table HumanResources.EmployeeAbsentee
	add constraint ck_Absent_EmployeeAbsentee check (Absent != Tardy)
;
go

-- Check Tardy != Absent
alter table HumanResources.EmployeeAbsentee
	add constraint ck_Tardy_EmployeeAbsentee check (Tardy != Absent)
;
go

/* ************************** JobTitle table ************************** */

/* ** table with no other constraints ** */


/* ************************** EmployeeSickDays table ************************** */
-- default DaysAllowed = 0
alter table HumanResources.EmployeeSickDays
	add constraint df_DaysAllowed_EmployeeSickDays default (0) for DaysAllowed
;
go

-- Check DaysTaken <= DaysAllowed
alter table HumanResources.EmployeeSickDays
	add constraint ck_DaysTaken_EmployeeSickDays check (DaysTaken <= DaysAllowed)
;
go

/* ************************** EmployeeVacationDays table ************************** */
-- default DaysAllowed = 0
alter table HumanResources.EmployeeVacationDays
	add constraint df_DaysAllowed_EmployeeVacationDays default (0) for DaysAllowed
;
go

-- Check DaysTaken <= DaysAllowed
alter table HumanResources.EmployeeVacationDays
	add constraint df_DaysTaken_EmployeeVacationDays check (DaysTaken <= DaysAllowed)
;
go

/* **************************************************** EmployeeTraining Schema **************************************************** */

/* ************************** EmployeeTraining table ************************** */

/* ** table with no other constraints ** */


/* ************************** Class table ************************** */
-- Check Cost > 0
alter table EmployeeTraining.Class
	add constraint ck_Tardy_Class check (Cost > 0)
;
go

/* **************************************************** Drugs Schema **************************************************** */

/* ************************** Drug Table ************************** */

/* ** table with no other constraints ** */


/* **************************************************** Prescription Schema **************************************************** */

/* ************************** Refill table ************************** */

/* ** table with no other constraints ** */


/* ************************** Rx table ************************** */
-- Default Quantity = 1
alter table Prescription.Rx
	add constraint df_Quantity_Rx default (0) for Quantity
;
go

-- Default Refills = 0
alter table Prescription.Rx
	add constraint df_Refills_Rx default (0) for Refills
;
go

-- Default AutoRefills = 0
alter table Prescription.Rx
	add constraint df_AutoRefills_Rx default (0) for AutoRefills
;
go

-- Default RefillsUsed = 0
alter table Prescription.Rx
	add constraint df_RefillsUsed_Rx default (0) for RefillsUsed
;
go

-- Check RefillsUsed <= Refills
alter table Prescription.Rx
	add constraint ck_RefillsUsed_Rx check (RefillsUsed <= Refills)
;
go

/* **************************************************** Customers Schema **************************************************** */

/* ************************** Customers table ************************** */

-- check gender M or F ???
alter table Customers.Customers
	add constraint ck_Gender_Customers check ( gender in ('M', 'm','F','f'))
;
go

--Regular expression Phonenumber
alter table Customers.Customers
	add constraint ck_PrimaryPhone_Customers check ( PrimaryPhone like '^\([0-9]^3\) [0-9]^3-[0-9]^4$')
;
go

alter table Customers.Customers
	add constraint ck_Fax_Customers check ( PrimaryPhone like '^\([0-9]^3\) [0-9]^3-[0-9]^4$')
;
go

/* ************************** CustomerDetails table ************************** */

alter table Customers.CustomerDetails
	add constraint ck_Sin_CustomerDetails check ( Sin like '^[0-9]^3-[0-9]^3-[0-9]^3$')
;
go


/* ************************** MaritalStatus table ************************** */

/* ** table with no other constraints ** */


/* ************************** HealthPlan table ************************** */
-- Unique InsuranceNo
alter table Customers.HealthPlan
	add constraint uq_InsuranceNo_HealthPlan unique (InsuranceNo)
;
go

-- Default CityId = Mtl
alter table Customers.HealthPlan
	add constraint df_CityID_HealthPlan default ('Mtl') for CityID
;
go

-- Default Province = Qc
alter table Customers.HealthPlan
	add constraint df_Province_HealthPlan default ('Qc') for Province
;
go

-- Default Country = Canada
alter table Customers.HealthPlan
	add constraint df_Country_HealthPlan default ('Canada') for Country
;
go
--Regular expression Phonenumber
alter table Customers.HealthPlan
	add constraint ck_PostalCode_HealthPlan check (PostalCode like '^[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][abceghjklmnprstvwxyzABCEGHJKLMNPRSTVWXYZ] [0-9][abceghjklmnprstvwxyzABCEGHJKLMNPRSTVWXYZ][0-9]$')
;
go
--Regular expression Phonenumber
alter table Customers.HealthPlan
	add constraint ck_Phone_HealthPlan check ( Phone like '^\([0-9]^3\) [0-9]^3-[0-9]^4$')
;
go

alter table Customers.HealthPlan
	add constraint ck_Province_HealthPlan check (Province in ('NL','PE','NS','NB', 'QC','ON', 'NU','MB', 'SK','AB', 'BC', 'YT', 'NT'))
;
go
/* ************************** Household table ************************** */

--Regular expression Phonenumber
alter table Customers.Household
	add constraint ck_HHPhone_Household check ( HHPhone like '^\([0-9]^3\) [0-9]^3-[0-9]^4$')
;
go


-- Default CityId = Mtl
alter table Customers.Household
	add constraint df_CityID_Household default ('Mtl') for CityID
;
go

-- Default Province = Qc
alter table Customers.Household
	add constraint df_Province_Household default ('Qc') for Province
;
go

-- Default Country = Canada
alter table Customers.Household
	add constraint df_Country_Household default ('Canada') for Country
;
go
--Regular expression PostalCode
alter table Customers.Household
	add constraint ck_PostalCode_Household check (PostalCode like '^[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][abceghjklmnprstvwxyzABCEGHJKLMNPRSTVWXYZ] [0-9][abceghjklmnprstvwxyzABCEGHJKLMNPRSTVWXYZ][0-9]$')
;
go

alter table Customers.Household
	add constraint ck_Province_Household check (Province in ('NL','PE','NS','NB', 'QC','ON', 'NU','MB', 'SK','AB', 'BC', 'YT', 'NT'))
;
go

/* ************************** Allergy table ************************** */

/* ** table with no other constraints ** */


/* **************************************************** Operations Schema **************************************************** */

/* ************************** Suppliers table ************************** */
--Regular expression Phonenumber
alter table Operations.Suppliers
	add constraint ck_Phone_Suppliers check ( Phone like '^\([0-9]^3\) [0-9]^3-[0-9]^4$')
;
go

-- Default CityId = Mtl
alter table Operations.Suppliers
	add constraint df_CityID_Suppliers default ('Mtl') for CityID
;
go

-- Default Province = Qc
alter table Operations.Suppliers
	add constraint df_Province_Suppliers default ('Qc') for Province
;
go

-- Default Country = Canada
alter table Operations.Suppliers
	add constraint df_Country_Suppliers default ('Canada') for Country
;
go

alter table Operations.Suppliers
	add constraint ck_Province_Suppliers check (Province in ('NL','PE','NS','NB', 'QC','ON', 'NU','MB', 'SK','AB', 'BC', 'YT', 'NT'))
;
go

--Regular expression PostalCode
alter table Operations.Suppliers
	add constraint ck_PostalCode_Suppliers check (PostalCode like '^[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][abceghjklmnprstvwxyzABCEGHJKLMNPRSTVWXYZ] [0-9][abceghjklmnprstvwxyzABCEGHJKLMNPRSTVWXYZ][0-9]$')
;
go

/* **************************************************** Medical Schema **************************************************** */

/* ************* Doctor table ************* */

--Regular expression Phonenumber
alter table Medical.Doctor
	add constraint ck_PrimaryPhone_Doctor check ( PrimaryPhone like '^\([0-9]^3\) [0-9]^3-[0-9]^4$')
;
go

alter table Medical.Doctor
	add constraint ck_SecondaryPhone_Doctor check ( SecondaryPhone like '^\([0-9]^3\) [0-9]^3-[0-9]^4$')
;
go


/* ************* Clinic table ************* */
-- Default CityId = Mtl
alter table Medical.Clinic
	add constraint df_CityID_Clinic default ('Mtl') for CityID
;
go

-- Default Province = Qc
alter table Medical.Clinic
	add constraint df_Province_Clinic default ('Qc') for Province
;
go

alter table Medical.Clinic
	add constraint ck_Province_Clinic check (Province in ('NL','PE','NS','NB', 'QC','ON', 'NU','MB', 'SK','AB', 'BC', 'YT', 'NT'))
;
go

-- Default Country = Canada
alter table Medical.Clinic
	add constraint df_Country_Clinic default ('Canada') for Country
;
go

--Regular expression Phonenumber
alter table Medical.Clinic
	add constraint ck_Phone_Clinic check ( Phone like '^\([0-9]^3\) [0-9]^3-[0-9]^4$')
;
go
--Regular expression PostalCode
alter table Medical.Clinic
	add constraint ck_PostalCode_Clinic check (PostalCode like '^[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY][0-9][abceghjklmnprstvwxyzABCEGHJKLMNPRSTVWXYZ] [0-9][abceghjklmnprstvwxyzABCEGHJKLMNPRSTVWXYZ][0-9]$')
;
go

/* **************************************************** DBO  Schema **************************************************** */

/* ************* DBO table ************* */

/* ** table with no other constraints ** */
