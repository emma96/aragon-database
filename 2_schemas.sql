/* Purpose: create schema object in the AragonPharmacy database
	Script Date: February 13, 2021
	Developed by: Emma
*/

-- This file is not executed yet!

use AragonPharmacy
;
go

/* 
Partial Syntax:
create schema schema_name authorization owner_name
*/

-- 1) create Customers schema
create schema Customers authorization dbo
;
go

-- 2) create Operations schema
create schema Operations authorization dbo
;
go

-- 3) create Medical schema
create schema Medical authorization dbo
;
go

-- 4) create Drugs schema
create schema Drugs authorization dbo
;
go

-- 5) create Prescription schema
create schema Prescription authorization dbo
;
go

--6) create HumanResources schema

create schema HumanResources;
go

-- 7) create EmployeeTraining schema

create schema EmployeeTraining;
go
